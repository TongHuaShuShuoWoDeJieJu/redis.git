package com.ghy.springredis;

import com.ghy.springredis.demo1.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author: 卢佳新
 * @version: v1.0
 * @description: com.ghy.springredis
 * @date:2022/6/10 23:26
 * @Motto 不积跬步，无以至千里；不积小流，无以成江海。
 */
@SpringBootTest
public class RedisDemoApplicationTest2 {

    // 注入 RedisTemplate
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Test
    void testString() {
        redisTemplate.opsForValue().set("name", "小白");
        Object name = redisTemplate.opsForValue().get("name");
        System.out.println(name);
    }

    @Test
    void testSaveUser() {
        redisTemplate.opsForValue().set("user", new User("小白", 23));
        User user = (User) redisTemplate.opsForValue().get("user");
        System.out.println(user);
    }
}
