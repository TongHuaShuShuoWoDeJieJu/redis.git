package com.ghy.springredis;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import redis.clients.jedis.Jedis;

import java.util.Map;

/**
 * @author: 卢佳新
 * @version: v1.0
 * @description: com.ghy.springredis
 * @date:2022/6/11 14:40
 * @Motto 不积跬步，无以至千里；不积小流，无以成江海。
 */
public class JedisTest {

    private Jedis jedis;

    @BeforeEach
    void setUp(){
        // 1、建立连接
        jedis = new Jedis("ip", 6379);
        // 2、设置密码
        jedis.auth("123456");
        // 3、选择库
        jedis.select(0);
    }


    @Test
    public void testString(){
        // 存入数据
        String result = jedis.set("name", "张三");
        System.out.println("result = " + result);
        // 获取数据
        String name = jedis.get("name");
        System.out.println(name);
    }

    @Test
    public void testHash(){
        // 插入 hash 数据
        jedis.hset("user:1", "name", "lisi");
        jedis.hset("user:1", "age", "21");

        // 获取
        Map<String, String> map = jedis.hgetAll("user:1");
        System.out.println(map);
    }

    @AfterEach
    void closeJedis(){
        if(jedis != null){
            jedis.close();
        }
    }
}

