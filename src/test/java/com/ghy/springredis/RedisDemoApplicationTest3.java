package com.ghy.springredis;

import com.alibaba.fastjson.JSON;
import com.ghy.springredis.demo1.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * @author: 卢佳新
 * @version: v1.0
 * @description: com.ghy.springredis
 * @date:2022/6/10 23:34
 * @Motto 不积跬步，无以至千里；不积小流，无以成江海。
 */
@SpringBootTest
public class RedisDemoApplicationTest3 {

    // 注入 StringRedisTemplate
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    void testSaveUser() {
        User user = new User("小白", 23);
        // 序列化
        stringRedisTemplate.opsForValue().set("user", JSON.toJSONString(user));
        String user1 = stringRedisTemplate.opsForValue().get("user");
        // 反序列化
        User user2 = JSON.parseObject(user1, User.class);
        System.out.println(user2);
    }
}
