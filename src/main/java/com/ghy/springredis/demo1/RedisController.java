package com.ghy.springredis.demo1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author: 卢佳新
 * @version: v1.0
 * @description: com.ghy.springredis
 * @date:2022/6/10 23:00
 * @Motto 不积跬步，无以至千里；不积小流，无以成江海。
 */
@CrossOrigin
@RestController
@RequestMapping(value = "redis")
public class RedisController {


    @Autowired
    RedisTemplate redisTemplate;

    @RequestMapping(value = "/index",method = { RequestMethod.GET}, produces = MediaType.APPLICATION_JSON_VALUE)
    public String index(){

        redisTemplate.opsForValue ().set ( "key","value" );
        return (String) redisTemplate.opsForValue ().get ( "key" );
    }
}
