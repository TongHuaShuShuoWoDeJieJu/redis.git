package com.ghy.springredis.demo1;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @author: 卢佳新
 * @version: v1.0
 * @description: com.ghy.springredis.demo1
 * @date:2022/6/10 23:25
 * @Motto 不积跬步，无以至千里；不积小流，无以成江海。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String name;
    private Integer age;
}
