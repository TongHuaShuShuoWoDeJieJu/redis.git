package com.ghy.springredis.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * @author: 卢佳新
 * @version: v1.0
 * @description: com.ghy.springredis.client
 * @date:2022/6/11 13:19
 * @Motto 不积跬步，无以至千里；不积小流，无以成江海。
 */
public class CustomJedisProd {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket();
        InetSocketAddress socketAddress = new InetSocketAddress("106.12.75.86", 6379);
        socket.connect(socketAddress, 10000);
        OutputStream outputStream = socket.getOutputStream();
        //验证密码
        outputStream.write(auth());
        InputStream inputStream = socket.getInputStream();
        byte[] buffer = new byte[1024];
        inputStream.read(buffer);
        System.out.println("返回执行结果：" + new String(buffer));
        //发送数据
        outputStream.write(command());
        inputStream.read(buffer);
        System.out.println("返回执行结果：" + new String(buffer));
        inputStream.close();
        outputStream.close();
    }
    //验证
    private static byte[] auth(){
        return "*2\r\n$4\r\nAUTH\r\n$6\r\n123456\r\n".getBytes();
    }
    //组装报文信息
    private static byte[] command() {
        return "*3\r\n$3\r\nSET\r\n$9\r\nuser:name\r\n$6\r\nitcrud\r\n".getBytes();
    }
}
