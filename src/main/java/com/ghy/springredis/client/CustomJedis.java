package com.ghy.springredis.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * @author: 卢佳新
 * @version: v1.0
 * @description: com.ghy.springredis.client
 * @date:2022/6/11 13:15
 * @Motto 不积跬步，无以至千里；不积小流，无以成江海。
 */
public class CustomJedis {
    public static void main(String[] args) throws IOException {
        //建立socket连接
        Socket socket = new Socket();
        InetSocketAddress socketAddress = new InetSocketAddress("106.12.75.86", 6379);
        socket.connect(socketAddress, 10000);
        //获取scoket输出流，将报文转换成byte[]传入流中
        OutputStream outputStream = socket.getOutputStream();
        outputStream.write(command());
        //获取返回的输出流，并打印输出数据
        InputStream inputStream = socket.getInputStream();
        byte[] buffer = new byte[1024];
        inputStream.read(buffer);
        System.out.println("返回执行结果：" + new String(buffer));
    }
    //组装报文信息
    private static byte[] command() {
        return "*3\r\n$3\r\nSET\r\n$9\r\nuser:name\r\n$6\r\nitcrud\r\n".getBytes();
    }
}

