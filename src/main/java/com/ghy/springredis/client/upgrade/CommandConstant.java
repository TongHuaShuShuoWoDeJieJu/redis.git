package com.ghy.springredis.client.upgrade;

/**
 * @author: 卢佳新
 * @version: v1.0
 * @description: com.ghy.springredis.client.upgrade
 * @date:2022/6/11 13:35
 * @Motto 不积跬步，无以至千里；不积小流，无以成江海。
 */
public class CommandConstant {

    public static final String START="*";

    public static final String LENGTH="$";

    public static final String LINE="\r\n";

    public enum CommandEnum{
        SET,
        GET
    }
}
