package com.ghy.springredis.client.upgrade;

/**
 * @author: 卢佳新
 * @version: v1.0
 * @description: com.ghy.springredis.client.upgrade
 * @date:2022/6/11 13:36
 * @Motto 不积跬步，无以至千里；不积小流，无以成江海。
 */
public class MainClient {
    public static void main(String[] args) {
        CustomerRedisClient customerRedisClient=new CustomerRedisClient("106.12.75.86",6379,"123456");
        System.out.println(customerRedisClient.set("name","ljx"));
        System.out.println(customerRedisClient.get("ljx"));
    }
}
