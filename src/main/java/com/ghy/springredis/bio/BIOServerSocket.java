package com.ghy.springredis.bio;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author: 卢佳新
 * @version: v1.0
 * @description: com.ghy.springredis.bio
 * @date:2022/6/14 22:10
 * @Motto 不积跬步，无以至千里；不积小流，无以成江海。
 */
public class BIOServerSocket {
    //先定义一个端口号，这个端口的值是可以自己调整的。
    static final int DEFAULT_PORT = 8080;
    public static void main(String[] args) {
        ServerSocket serverSocket=null;

        try {
            serverSocket=new ServerSocket(DEFAULT_PORT);
            System.out.println("启动服务：监听端口:8080");
            //表示阻塞等待监听一个客户端连接,返回的socket表示连接的客户端信息
            while(true) {
                Socket socket = serverSocket.accept(); //连接阻塞
                System.out.println("客户端：" + socket.getPort());
                //inputstream是阻塞的(***)
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream())); //表示获取客户端的请求报文
                String clientStr = bufferedReader.readLine();
                System.out.println("收到客户端发送的消息：" + clientStr);
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                bufferedWriter.write("receive a message:" + clientStr + "\n");
                bufferedWriter.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(serverSocket!=null){
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
