package com.ghy.springredis.bio;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author: 卢佳新
 * @version: v1.0
 * @description: com.ghy.springredis.bio
 * @date:2022/6/19 16:21
 * @Motto 不积跬步，无以至千里；不积小流，无以成江海。
 */
public class BIOServerSocketWithThread {

    static ExecutorService executorService= Executors.newFixedThreadPool (10);
    public static void main(String[] args) {
        ServerSocket serverSocket=null;
        try{
            serverSocket=new ServerSocket ( 8080 );
            System.out.println("启动服务：监听端口:8080");
            while (true){
                Socket socket = serverSocket.accept(); //连接阻塞
                System.out.println("客户端：" + socket.getPort());
                //IO变成了异步执行
                executorService.submit ( new SocketThread (socket) );
            }
        }catch (IOException e){
            e.printStackTrace ();
        }finally {
            if (serverSocket!=null){
                try {
                    serverSocket.close ();
                }catch (IOException e){
                    e.printStackTrace ();
                }
            }


        }
    }
}
