package com.ghy.springredis.reactor;

import java.io.IOException;

/**
 * @author: 卢佳新
 * @version: v1.0
 * @description: com.ghy.springredis.reactor
 * @date:2022/6/19 19:25
 * @Motto 不积跬步，无以至千里；不积小流，无以成江海。
 */
public class ReactorMain {
    public static void main(String[] args) throws IOException {
        new Thread(new Reactor(8080),"Main-Thread").start();
    }
}
